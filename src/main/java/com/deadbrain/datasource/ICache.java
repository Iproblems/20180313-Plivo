package com.deadbrain.datasource;

public interface ICache {
    boolean containsFromToPair(String pair);

    void saveFromToPair(String pair);

    boolean isRequestUnderLimit(String from);

    boolean checkHealth();
}
