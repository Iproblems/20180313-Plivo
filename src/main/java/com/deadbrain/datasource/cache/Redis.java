package com.deadbrain.datasource.cache;

import com.deadbrain.configuration.AppConfiguration;
import com.deadbrain.datasource.ICache;
import com.google.inject.Inject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

public class Redis implements ICache {

    AppConfiguration appConfiguration;

    @Inject
    public Redis(AppConfiguration appConfiguration) {
        this.appConfiguration = appConfiguration;
    }

    @Override
    public boolean containsFromToPair(String pair) {
        try (Jedis jedis = new Jedis(
                appConfiguration.getProperty(AppConfiguration.REDIS_HOST),
                Integer.parseInt(appConfiguration.getProperty(AppConfiguration.REDIS_PORT)))) {
            return jedis.get(pair) != null;
        }
    }

    @Override
    public void saveFromToPair(String pair) {
        try (Jedis jedis = new Jedis(
                appConfiguration.getProperty(AppConfiguration.REDIS_HOST),
                Integer.parseInt(appConfiguration.getProperty(AppConfiguration.REDIS_PORT)))) {
            jedis.set(pair, "cached value");
            jedis.expire(pair, Integer.parseInt(appConfiguration.getProperty(AppConfiguration.PAIR_TIMEOUT)) * 60 * 60);
        }
    }

    @Override
    public boolean isRequestUnderLimit(String from) {
        try (Jedis jedis = new Jedis(
                appConfiguration.getProperty(AppConfiguration.REDIS_HOST),
                Integer.parseInt(appConfiguration.getProperty(AppConfiguration.REDIS_PORT)))) {
            if (jedis.get(from) == null) {
                Transaction transaction = jedis.multi();
                transaction.set(from, appConfiguration.getProperty(AppConfiguration.REQUEST_LIMIT));
                transaction.expire(from, 60 * 60 * Integer.parseInt(appConfiguration.getProperty(AppConfiguration
                        .REQUEST_TIMEOUT)));
                transaction.exec();
            }
            if (Integer.parseInt(jedis.get(from)) <= 0)
                return false;
            jedis.decr(from);
            return true;
        }
    }

    @Override
    public boolean checkHealth() {
        try (Jedis jedis = new Jedis(
                appConfiguration.getProperty(AppConfiguration.REDIS_HOST),
                Integer.parseInt(appConfiguration.getProperty(AppConfiguration.REDIS_PORT)))) {
            return jedis.ping() != null && jedis.ping().trim().equals("PONG");
        }
    }
}
