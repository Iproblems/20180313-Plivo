package com.deadbrain.controller;

import com.deadbrain.controller.request.SmsRequest;
import com.deadbrain.controller.response.SmsResponse;
import com.deadbrain.datasource.ICache;
import com.deadbrain.services.SmsService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Route;

import javax.servlet.http.HttpServletResponse;

public class OutboundSmsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InboundSmsController.class);
    private SmsService smsService;
    private ICache cache;

    @Inject
    public OutboundSmsController(SmsService smsService, ICache cache) {
        this.smsService = smsService;
        this.cache = cache;
    }

    public Route outgoingSms() {
        return (request, response) -> {
            LOGGER.trace("Inside outgoingSms");

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            SmsRequest smsRequest = objectMapper.readValue(request.body(), SmsRequest.class);

            SmsResponse smsResponse = smsService.isValid(smsRequest);
            if (smsResponse != null) {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return smsResponse;
            }

            if(!cache.isRequestUnderLimit(smsRequest.getFrom())){
                response.status(429);
                return new SmsResponse("", String.format("limit reached for from %s",
                        smsRequest.getFrom()));
            }

            if (cache.containsFromToPair(String.format("%s::%s",
                    smsRequest.getFrom(),
                    smsRequest.getTo()))) {
                response.status(HttpServletResponse.SC_CONFLICT);
                return new SmsResponse("", String.format("sms from %s and to %s blocked by STOP request",
                        smsRequest.getFrom(), smsRequest.getTo()));
            }

            return new SmsResponse("outbound sms is ok","");
        };
    }
}
