package com.deadbrain.controller;

import com.deadbrain.services.HealthCheckService;
import com.google.inject.Inject;
import spark.Route;

import javax.servlet.http.HttpServletResponse;

public class PingController {

    private HealthCheckService healthcheckService;

    @Inject
    public PingController(HealthCheckService healthcheckService) {
        this.healthcheckService = healthcheckService;
    }

    public Route ping() {
        return (request, response) -> {
            String ping = healthcheckService.ping();
            if(!ping.equals("pong")){
                response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
            return ping;
        };
    }
}
