package com.deadbrain.controller.response;

import java.util.Objects;

public class SmsResponse {
    private String message;
    private String error;

    public SmsResponse(String message, String error) {
        this.message = message;
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SmsResponse)) return false;
        SmsResponse that = (SmsResponse) o;
        return Objects.equals(getMessage(), that.getMessage()) &&
                Objects.equals(getError(), that.getError());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getMessage(), getError());
    }
}
