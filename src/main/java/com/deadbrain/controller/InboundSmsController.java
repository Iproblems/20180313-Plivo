package com.deadbrain.controller;

import com.deadbrain.controller.request.SmsRequest;
import com.deadbrain.controller.response.SmsResponse;
import com.deadbrain.services.SmsService;
import com.deadbrain.datasource.ICache;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Route;

import javax.servlet.http.HttpServletResponse;

public class InboundSmsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InboundSmsController.class);
    private SmsService smsService;
    private ICache cache;

    @Inject
    public InboundSmsController(SmsService smsService, ICache cache) {
        this.smsService = smsService;
        this.cache = cache;
    }

    public Route incomingSms() {
        return (request, response) -> {
            LOGGER.trace("Inside incomingSms");

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            SmsRequest smsRequest = objectMapper.readValue(request.body(), SmsRequest.class);

            SmsResponse smsResponse = smsService.isValid(smsRequest);
            if (smsResponse != null) {
                response.status(HttpServletResponse.SC_BAD_REQUEST);
                return smsResponse;
            }

            if (smsService.isStopInTest(smsRequest.getText())) {
                    cache.saveFromToPair(String.format("%s::%s",
                            smsRequest.getFrom(),
                            smsRequest.getTo()));
            }

            return new SmsResponse("inbound sms is ok","");
        };
    }
}

