package com.deadbrain.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {
    private ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonTransformer.class);


    @Override
    public String render(Object model) {

        String jsonStr = null;
        try {
            jsonStr = objectMapper.writeValueAsString(model);
        } catch (Exception ex) {
            LOGGER.error("Failed at json transformation", ex);
        }

        return jsonStr;
    }
}
