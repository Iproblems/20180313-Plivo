package com.deadbrain.dependency;

import com.deadbrain.configuration.AppConfiguration;
import com.deadbrain.controller.InboundSmsController;
import com.deadbrain.controller.OutboundSmsController;
import com.deadbrain.controller.PingController;
import com.deadbrain.datasource.ICache;
import com.deadbrain.datasource.cache.Redis;
import com.deadbrain.services.HealthCheckService;
import com.deadbrain.services.SmsService;
import com.google.inject.AbstractModule;

public class DependencyModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(AppConfiguration.class);
        bind(InboundSmsController.class);
        bind(PingController.class);
        bind(HealthCheckService.class);
        bind(ICache.class).to(Redis.class);
        bind(SmsService.class);
        bind(OutboundSmsController.class);
    }
}
