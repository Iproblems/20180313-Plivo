package com.deadbrain.configuration;

import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.yaml.snakeyaml.Yaml;

import java.util.Map;
import java.util.Properties;

@Singleton
public class AppConfiguration {
    public static final String PAIR_TIMEOUT = "PAIR_TIMEOUT";
    public static final String BASIC_AUTH_USERNAME = "BASIC_AUTH_USERNAME";
    public static final String BASIC_AUTH_PASSWORD = "BASIC_AUTH_PASSWORD";
    public static final String REDIS_HOST="REDIS_HOST";
    public static final String REDIS_PORT="REDIS_PORT";
    public static final String SERVICE_PORT = "SERVICE_PORT";
    public static final String REQUEST_TIMEOUT= "REQUEST_TIMEOUT";
    public static final String REQUEST_LIMIT = "REQUEST_LIMIT";
    private static final String PROP_FILE_NAME = "application.yaml";
    private final Properties properties;

    @Inject
    private AppConfiguration() {
        properties = new Properties();
        Yaml yaml = new Yaml();
        Map<String, String> values = yaml
                .load(AppConfiguration.class.getClassLoader().getResourceAsStream(PROP_FILE_NAME));
        for (Map.Entry<String,String> entry : values.entrySet()) {
            properties.setProperty(entry.getKey(), entry.getValue());
        }
        overWriteOSProperties();
    }

    @Provides
    @Singleton
    public static AppConfiguration getInstance() {
        return new AppConfiguration();
    }

    private void overWriteOSProperties() {
        properties.keySet().forEach(property -> {
            String osValue = System.getenv((String) property);
            if (osValue != null && !osValue.isEmpty())
                properties.setProperty((String) property, osValue);
        });
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }


}
