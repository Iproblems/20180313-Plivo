package com.deadbrain.services;

import com.deadbrain.controller.request.SmsRequest;
import com.deadbrain.controller.response.SmsResponse;

public class SmsService {

    public SmsResponse isValid(SmsRequest smsRequest) {
        if (smsRequest == null)
            return new SmsResponse("", "Request is null");
        if (smsRequest.getFrom() == null)
            return new SmsResponse("", "from is missing");
        if (smsRequest.getFrom().trim().length() < 6 || smsRequest.getFrom().trim().length() > 16) {
            return new SmsResponse("", "from is invalid");
        }
        if (smsRequest.getTo() == null)
            return new SmsResponse("", "to is missing");
        if (smsRequest.getTo().trim().length() < 6 || smsRequest.getTo().trim().length() > 16) {
            return new SmsResponse("", "to is invalid");
        }
        if (smsRequest.getText() == null)
            return new SmsResponse("", "text is missing");
        if (smsRequest.getTo().trim().length() > 120) {
            return new SmsResponse("", "text is invalid");
        }

        return null;
    }

    public boolean isStopInTest(String text){
        return text.contains("STOP");
    }

}
