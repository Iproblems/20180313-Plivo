package com.deadbrain.services;

import com.deadbrain.datasource.ICache;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealthCheckService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HealthCheckService.class);
    private final ICache cache;

    @Inject
    public HealthCheckService(ICache cache) {
        this.cache = cache;
    }

    public String ping() {
        if (cache.checkHealth()) {
            LOGGER.info("Successfully checked connectivity with the redis");
            return "pong";
        }

        LOGGER.warn("Could not get the correct response from the redis");
        return "Something went wrong";
    }
}
