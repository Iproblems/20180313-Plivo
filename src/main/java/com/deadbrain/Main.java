package com.deadbrain;

import com.deadbrain.configuration.AppConfiguration;
import com.deadbrain.controller.OutboundSmsController;
import com.deadbrain.controller.PingController;
import com.deadbrain.controller.InboundSmsController;
import com.deadbrain.dependency.DependencyModule;
import com.deadbrain.utils.JsonTransformer;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.qmetric.spark.authentication.AuthenticationDetails;
import com.qmetric.spark.authentication.BasicAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.post;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new DependencyModule());
        AppConfiguration appConfiguration = injector.getInstance(AppConfiguration.class);
        PingController pingController = injector.getInstance(PingController.class);

        Spark.port(Integer.parseInt(appConfiguration.getProperty(AppConfiguration.SERVICE_PORT)));
        LOGGER.info("Starting Server at {} ....... ", appConfiguration.getProperty(AppConfiguration.SERVICE_PORT));

        LOGGER.info("Binding Apis");
        InboundSmsController inboundSmsController = injector.getInstance(InboundSmsController.class);
        OutboundSmsController outboundSmsController = injector.getInstance(OutboundSmsController.class);
        String basicAuthUserName = appConfiguration.getProperty(AppConfiguration.BASIC_AUTH_USERNAME);
        String basicAuthPassword = appConfiguration.getProperty(AppConfiguration.BASIC_AUTH_PASSWORD);
        before(new BasicAuthenticationFilter("/*", new AuthenticationDetails(basicAuthUserName, basicAuthPassword)));
        get("/ping", pingController.ping());
        post("/inbound/sms", inboundSmsController.incomingSms(), new JsonTransformer());
        post("/outbound/sms", outboundSmsController.outgoingSms(), new JsonTransformer());
    }
}
