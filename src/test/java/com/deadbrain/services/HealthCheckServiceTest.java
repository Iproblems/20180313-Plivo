package com.deadbrain.services;

import com.deadbrain.datasource.ICache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class HealthCheckServiceTest {

    private HealthCheckService healthCheckService;
    private ICache cache;

    @BeforeEach
    void setUp() {
        cache = Mockito.mock(ICache.class);
        healthCheckService = new HealthCheckService(cache);
    }

    @Test
    void shouldPass() {
        Mockito.when(cache.checkHealth()).thenReturn(true);
        Assertions.assertEquals("pong", healthCheckService.ping().toLowerCase());
    }

    @Test
    void shouldFail(){
        Mockito.when(cache.checkHealth()).thenReturn(false);
        Assertions.assertNotEquals("pong", healthCheckService.ping().toLowerCase());
    }
}