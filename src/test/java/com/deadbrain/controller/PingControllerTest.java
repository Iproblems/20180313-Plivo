package com.deadbrain.controller;

import com.deadbrain.services.HealthCheckService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import spark.Request;
import spark.Response;

class PingControllerTest {
    private HealthCheckService healthcheckService;
    private PingController pingController;
    private Request request;
    private Response response;

    @BeforeEach
    void setUp() {
        healthcheckService = Mockito.mock(HealthCheckService.class);
        pingController = new PingController(healthcheckService);
        request = Mockito.mock(Request.class);
        response = Mockito.mock(Response.class);
    }

    @Test
    void shouldFail() throws Exception {
        Mockito.when(healthcheckService.ping()).thenReturn("String");
        Object handle = pingController.ping().handle(request, response);
        Assertions.assertNotNull(handle);
        Assertions.assertNotEquals("PONG",handle);
    }

}