package com.deadbrain.controller;


import com.deadbrain.controller.response.SmsResponse;
import com.deadbrain.datasource.ICache;
import com.deadbrain.datasource.cache.Redis;
import com.deadbrain.services.SmsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import spark.Request;
import spark.Response;


class InboundSmsControllerTest {
    private SmsService smsService;
    private ICache cache;
    private InboundSmsController inboundSmsController;
    private Request request;
    private Response response;

    @BeforeEach
    void setUp() {
        cache = Mockito.mock(Redis.class);
        smsService = Mockito.mock(SmsService.class);
        inboundSmsController = new InboundSmsController(smsService, cache);
        request = Mockito.mock(Request.class);
        response = Mockito.mock(Response.class);
    }

    @Test
    void shouldReturnInValidText() throws Exception {
        SmsResponse smsResponse = new SmsResponse("","text is invalid");
        Mockito.when(smsService.isValid(ArgumentMatchers.any())).thenReturn(smsResponse);
        Mockito.when(request.body()).thenReturn("{\"from\" : \"67656655777\",\"to\" : \"67656655777\",\"text\" : " +
                "\"this is a test\"}");
        Object handle = inboundSmsController.incomingSms().handle(request, response);
        Assertions.assertTrue(handle instanceof SmsResponse);
        SmsResponse apiResponse = (SmsResponse) handle;
        Assertions.assertEquals(apiResponse, smsResponse);
    }

    @Test
    void shouldBlockByStopText() throws Exception {
        Mockito.when(smsService.isValid(ArgumentMatchers.any())).thenReturn(null);
        Mockito.when(request.body()).thenReturn("{\"from\" : \"67656655777\",\"to\" : \"67656655777\",\"text\" : " +
                "\"this is a test\"}");
        Mockito.when(smsService.isStopInTest(ArgumentMatchers.any())).thenReturn(true);
        Mockito.when(cache.containsFromToPair(ArgumentMatchers.any())).thenReturn(true);
        Object handle = inboundSmsController.incomingSms().handle(request, response);
        Assertions.assertNotNull(handle);
        Assertions.assertTrue(handle instanceof SmsResponse);
        SmsResponse apiResponse = (SmsResponse) handle;
        Assertions.assertEquals("inbound sms is ok", apiResponse.getMessage());
    }

    @Test
    void shouldValidateApiLimiter() throws Exception {
        Mockito.when(smsService.isValid(ArgumentMatchers.any())).thenReturn(null);
        Mockito.when(request.body()).thenReturn("{\"from\" : \"67656655777\",\"to\" : \"67656655777\",\"text\" : " +
                "\"this is a test\"}");
        Mockito.when(smsService.isStopInTest(ArgumentMatchers.any())).thenReturn(false);
        Mockito.when(cache.isRequestUnderLimit(ArgumentMatchers.any())).thenReturn(false);
        Object handle = inboundSmsController.incomingSms().handle(request, response);
        Assertions.assertNotNull(handle);
        Assertions.assertTrue(handle instanceof SmsResponse);
        SmsResponse apiResponse = (SmsResponse) handle;
        Assertions.assertEquals("inbound sms is ok", apiResponse.getMessage());
    }
}