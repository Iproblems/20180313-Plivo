package com.deadbrain.configuration;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AppConfigurationTest {

    @Test
    void shouldValidateThePropertiesFromPropertyFile(){
        String dbUserName = "50";
        AppConfiguration appConfiguration = AppConfiguration.getInstance();
        assertEquals(dbUserName, appConfiguration.getProperty("REQUEST_LIMIT"));
    }

}