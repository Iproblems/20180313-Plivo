### Setup

#### Install Redis
- `brew install redis`  - skip if already installed
- `redis-server` to start the server

#### Building

- run `./gradlew clean build`
- DB Setup - run the migration `java -jar build/libs/plivo-1.0-SNAPSHOT.jar`